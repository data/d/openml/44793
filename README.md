# OpenML dataset: ECG5000

https://www.openml.org/d/44793

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The original dataset for 'ECG5000' is a 20-hour long ECG downloaded from Physionet. The name is BIDMC Congestive Heart Failure Database(chfdb) and it is record 'chf07'. It was originally published in 'Goldberger AL, Amaral LAN, Glass L, Hausdorff JM, Ivanov PCh, Mark RG, Mietus JE, Moody GB, Peng C-K, Stanley HE. PhysioBank, PhysioToolkit, and PhysioNet: Components of a New Research Resource for Complex Physiologic Signals. Circulation 101(23)'. The data was pre-processed in two steps: (1) extract each heartbeat, (2) make each heartbeat equal length using interpolation. This dataset was originally used in paper 'A general framework for never-ending learning from time series streams', DAMI 29(6). After that, 5,000 heartbeats were randomly selected. The patient has severe congestive heart failure and the class values were obtained by automated annotation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44793) of an [OpenML dataset](https://www.openml.org/d/44793). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44793/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44793/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44793/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

